/****************************************************************************
* COSC1252/1254 Programming Using C++
* SP1 2016 Assignment #1 - Mazes
* Full Name        : Paolo Di Pietro
* Student Number   : s3427174
****************************************************************************/
#pragma once

#include <iostream>
#include <fstream>
#include <vector>
#include <exception>
#include "edge.h"

using namespace std;

// the first 3 values in the binary file correspond to the width, height 
// and number of edges, so we start from the 4th index to begin pushing 
// structs into the vector
const int firstStruct = 3;   
// for loop counter variable iterates the loop from the 4th cell which 
// corresponds to the start of a new Edge struct
const int nextEdge = 4;
// these constants are for when user loads a maze larger than 
// 1000 width or height 
const int maxWidth = 1000;
const int maxHeight = 1000;

class Maze {

vector<Edge::EdgeType> edges;
vector<unsigned int> co_ords;

private: 
	unsigned int width;
	unsigned int height;

public:
	Maze(int a, int b) : width(a), height(b) {}
	Maze() {}

	vector<Edge::EdgeType> getEdgeVector() { return edges; }
	unsigned int getWidth() { return width; }
	unsigned int getHeight() { return height; }
   vector<Edge::EdgeType> loadMaze(string filename);
   void validateMaze(vector<unsigned int>& co_ords, unsigned int width, 
   													 unsigned int height);
};
