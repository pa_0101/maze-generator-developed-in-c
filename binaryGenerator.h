/****************************************************************************
* COSC1252/1254 Programming Using C++
* SP1 2016 Assignment #1 - Mazes
* Full Name        : Paolo Di Pietro
* Student Number   : s3427174
****************************************************************************/
#pragma once

#include <random>
#include "edge.h"
#include "cell.h"
#include "maze.h"

const unsigned int east = 0;
const unsigned int south = 1;

class BinaryGenerator {

public:
	BinaryGenerator () {}

	vector<Edge::EdgeType> generateMaze(unsigned int& seed, Maze m);
};
