/****************************************************************************
* COSC1252/1254 Programming Using C++
* SP1 2016 Assignment #1 - Mazes
* Full Name        : Paolo Di Pietro
* Student Number   : s3427174
****************************************************************************/
#include "svg.h"

bool SVG::saveToSVG(vector<Edge::EdgeType>& edges, unsigned int& width, 
												               unsigned int& height,
												               string filename)
{
	ofstream fout;
	
	fout.open(filename);

	if (fout.fail())
	{
		cout << "Not able to open SVG";
		return false;
	}

	// print the svg file attributes at the top of the file
	fout << "<svg viewBox='0 0 1 1' width='500' height='500'" 
			  " xmlns='http://www.w3.org/2000/svg'>" << endl;
	fout << "	<rect width='1' height='1' style='fill: black'/>" << endl;

	// loop through the coords vector and put each value into 
	// its corresponding coordinate in the SVG file
	for(unsigned int i = 0; i < edges.size(); i++)
	{
		// divide by the width and height and cast to a double to be used as 
		// the values in the coordinate attributes
		double x1 = (double)edges[i].x1/width;
		double y1 = (double)edges[i].y1/height;
		double x2 = (double)edges[i].x2/width;
		double y2 = (double)edges[i].y2/height;

		// setprecision function is used to format my floating point 
		// values to 6 decimal place
		fout << "		<line stroke='white' stroke-width='0.005' x1='" 
			            << std::setprecision(6) << x1 << "' ";
		fout << "y1='" << std::setprecision(6) << y1 << "' ";
		fout << "x2='" << std::setprecision(6) << x2 << "' ";
		fout << "y2='" << std::setprecision(6) << y2 << "'/>" << endl; 
	}

	fout << "</svg>" << endl;

	fout.close();

	return true;
}
