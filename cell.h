/****************************************************************************
* COSC1252/1254 Programming Using C++
* SP1 2016 Assignment #1 - Mazes
* Full Name        : Paolo Di Pietro
* Student Number   : s3427174
****************************************************************************/
#pragma once

#include <iostream>
#include <vector>

using namespace std;
using std::vector;

class Cell {

private:

	vector<vector<Cell>> cells;
	unsigned int x;
	unsigned int y;

	typedef struct adjacentCell {
		unsigned int xEast;
		unsigned int yEast;
		unsigned int xSouth;
		unsigned int ySouth;
	} AdjacentCellType;

	AdjacentCellType myAdjacentCell;
  
public:
	Cell() : x(0), y(0) {}

	vector<vector<Cell>> getCellVector() { return cells; }
	unsigned int getX() { return x; }
	unsigned int getY() { return y; }
	AdjacentCellType getAdjCellType() { return myAdjacentCell; }
	void initCellCoords(unsigned int& width, unsigned int& height);
	void getAdjacentCell(unsigned int& width, unsigned int& height, 
								vector<vector<Cell>>& cells);
	void initAdjList(vector<int>& adjCells, 
						  vector<vector<Cell>>& cells, 
						  unsigned int& width, unsigned int& height);
	void printCells(vector<vector<Cell>>& cells);
};
