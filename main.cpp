/****************************************************************************
* COSC1252/1254 Programming Using C++
* SP1 2016 Assignment #1 - Mazes
* Full Name        : Paolo Di Pietro
* Student Number   : s3427174
****************************************************************************/
#include <time.h>
#include "commandLine.h"

int main(int argc, char *argv[]) {

	clock_t start, end;
	CommandLine c;

	start = clock();

	c.commandValidator(argc, argv);
	
	end = clock();

	cout << "Execution time: " << (double)(end-start)/CLOCKS_PER_SEC
											  << " seconds." << endl;

	return EXIT_SUCCESS;
}
