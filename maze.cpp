/****************************************************************************
* COSC1252/1254 Programming Using C++
* SP1 2016 Assignment #1 - Mazes
* Full Name        : Paolo Di Pietro
* Student Number   : s3427174
****************************************************************************/
#include "maze.h"

// loadMaze function opens the binary file and casts the binary numbers 
// in the file to integers which are then pushed into the co_ords vector.
// We then loop through the co_ords vector and asign the integers to each 
// corresponding EdgeType struct member. The edge struct is then pushed into
// the Edge vector.
vector<Edge::EdgeType> Maze::loadMaze(string filename)
{
	Edge::EdgeType edge;
	ifstream file;
	unsigned int num;

   file.open(filename, ios::binary);

   if(file.fail())
	{
		cout << "Filename does not exist, not able to open binary file" << endl;
		throw std::exception();
	}   

	// loop through the file, cast each binary sequence to an integer
	// and push it into the vector
	while(file.read((char*)&num, sizeof(num)))
	{
		co_ords.push_back(num);
	}
	file.close();

	// get the width and height out the coords vector and copy them to
	// maze members. These are needed when we write to SVG
	this->width = co_ords[0];
	this->height = co_ords[1];
	
	validateMaze(co_ords, width, height);

	// loop through co-ords vector and assign each integer to an Edge struct
	// member. Push each struct into the Edge vector
	for(unsigned int i = firstStruct; i < co_ords.size(); i = i + nextEdge)
	{
		edge.x1 = co_ords[i];
		edge.y1 = co_ords[i + 1];
		edge.x2 = co_ords[i + 2];
		edge.y2 = co_ords[i + 3];
		edges.push_back(edge);
	}

	return edges;
}

// validateMaze function checks mazes for bad edges and heights
// in the binary maze files that were provided
void Maze::validateMaze(vector<unsigned int>& co_ords, unsigned int width, 
   													 unsigned int height)
{
	for(unsigned int i = firstStruct; i < co_ords.size(); i = i + nextEdge)
	{
		if(co_ords[i] > width || co_ords[i + 1] > height || 
			  width > maxWidth || height > maxHeight)
		{
			cout << "You have tried to load a bad maze....." << endl;
		   throw std::exception();
		}
	}
}
