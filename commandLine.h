/****************************************************************************
* COSC1252/1254 Programming Using C++
* SP1 2016 Assignment #1 - Mazes
* Full Name        : Paolo Di Pietro
* Student Number   : s3427174
****************************************************************************/
#pragma once

#include "maze.h"
#include "binaryGenerator.h"
#include "binary.h"
#include "svg.h"

class CommandLine {

public:
	CommandLine() {}

	void commandValidator(int& argc, char *argv[]);
	void genSaveToBin(unsigned int& seed, string width, 
					string height, string fileName);
	void loadBinSaveToSVG(string binaryFile, string svgfFileName);
	void genSaveToSVG(unsigned int& seed, string width, string height, 
																string svgFileName);
	void genSaveToBinSVG(unsigned int& seed, string width, 
						       string height, string fileName, string svgFileName);
	void printError();
};
