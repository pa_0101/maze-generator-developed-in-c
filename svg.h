/****************************************************************************
* COSC1252/1254 Programming Using C++
* SP1 2016 Assignment #1 - Mazes
* Full Name        : Paolo Di Pietro
* Student Number   : s3427174
****************************************************************************/
#pragma once

#include <vector>
#include <fstream>
#include <iomanip>
#include <iostream>
#include "edge.h"

using namespace std;
using std::vector;

class SVG {

public:
	SVG() {}

	bool saveToSVG(vector<Edge::EdgeType> &edges, unsigned int& width, 
												             unsigned int& height,
												             string filename);
};
