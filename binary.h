/****************************************************************************
* COSC1252/1254 Programming Using C++
* SP1 2016 Assignment #1 - Mazes
* Full Name        : Paolo Di Pietro
* Student Number   : s3427174
****************************************************************************/
#pragma once

#include <iostream>
#include <fstream>
#include <vector>
#include "edge.h"
#include "maze.h"

using namespace std;

class Binary {

public:
	
	Binary() {}

	void writeBinaryFile(Maze m, vector<Edge::EdgeType>& edges, 
																string filename);
};
