/****************************************************************************
* COSC1252/1254 Programming Using C++
* SP1 2016 Assignment #1 - Mazes
* Full Name        : Paolo Di Pietro
* Student Number   : s3427174
****************************************************************************/
#include "binary.h"

void Binary::writeBinaryFile(Maze m, vector<Edge::EdgeType>& edges, 
																		string filename)
{
	unsigned int num;

	// create output file stream
	ofstream file(filename, ios::binary);

	unsigned int width = m.getWidth(); 
	unsigned int height = m.getHeight(); 

	// write the width, height and number of edges as the 1st 3 binary 
	// values in the file
	num = width;
	file.write((char *)&num, sizeof(num));
	num = height;
	file.write((char *)&num, sizeof(num));
	num = edges.size();
	file.write((char *)&num, sizeof(num));

	// write the edges in binary to the file
	for(unsigned int i = 0; i < edges.size(); i++)
	{
      num = edges[i].x1;
      file.write((char *)&num, sizeof(num));
      num = edges[i].y1;
      file.write((char *)&num, sizeof(num));
      num = edges[i].x2;
      file.write((char *)&num, sizeof(num));
      num = edges[i].y2;
      file.write((char *)&num, sizeof(num));
   }
   
   file.close (); 
}
