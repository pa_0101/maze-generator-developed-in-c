/****************************************************************************
* COSC1252/1254 Programming Using C++
* SP1 2016 Assignment #1 - Mazes
* Full Name        : Paolo Di Pietro
* Student Number   : s3427174
****************************************************************************/
#include "commandLine.h"

// commandValidator function performs user input validation and calls the 
// various commands corresponding to command line flags
void CommandLine::commandValidator(int& argc, char *argv[])
{
	string str1 ("./mazer");
   string str2 ("--lb");
	string str3 ("--sb");
	string str4 ("--sv");
	string str5 ("--g");

	// check for correct exe file in first command line arg
	if(str1.compare(argv[0]) != 0)
	{
		cout << "No such file or directory" << endl;
	}

	// generate maze with seed, width and height values, save to binary file
	// ./mazer --g seed width height --sb filename.maze
	else if(str5.compare(argv[1]) == 0 && str3.compare(argv[5]) == 0 
												  && argc == 7)
	{
		unsigned int seedInt = stoi(argv[2]);
		genSaveToBin(seedInt, argv[3], argv[4], argv[6]);
	}

	// generate maze with time based seed, width and height values, save 
	// to binary file
	// ./mazer --g seed width height --sb filename.maze
	else if(str5.compare(argv[1]) == 0 && str3.compare(argv[4]) == 0 
												  && argc == 6)
	{
		// mersenne twister seeded with random number based on current time
		mt19937 mt_rand(time(0));
		unsigned int seedInt = mt_rand();
		cout << "Time based seed is: " << seedInt << endl;
		genSaveToBin(seedInt, argv[2], argv[3], argv[5]);
	}

	// load binary file and save to svg file 
	// ./mazer --lb filename.maze --sv filename.svg
	else if(str2.compare(argv[1]) == 0 && str4.compare(argv[3]) == 0
												  && argc == 5)
	{	
		loadBinSaveToSVG(argv[2], argv[4]);
	}

	// generate maze with seed, width and height values, save to svg file
	// ./mazer --g seed width height --sv filename.svg
	else if(str5.compare(argv[1]) == 0 && str4.compare(argv[5]) == 0 
												  && argc == 7)
	{
		unsigned int seedInt = stoi(argv[2]);
		genSaveToSVG(seedInt, argv[3], argv[4], argv[6]);
	}

	// generate maze with time based seed, width and height values, save 
	// to svg file
	// ./mazer --g seed width height --sv filename.maze
	else if(str5.compare(argv[1]) == 0 && str4.compare(argv[4]) == 0 
												  && argc == 6)
	{
		// mersenne twister seeded with random number based on current time
		mt19937 mt_rand(time(0));
		unsigned int seedInt = mt_rand();
		cout << "Time based seed is: " << seedInt << endl;
		genSaveToSVG(seedInt, argv[2], argv[3], argv[5]);
	}

	// generate maze with seed, width and height values, 
	// save to binary file and svg file
	// ./mazer --g seed width height --sb filename.maze --sv filename.svg
	else if(str5.compare(argv[1]) == 0 && str3.compare(argv[5]) == 0 
		  && str4.compare(argv[7]) == 0 && argc == 9)
	{
		unsigned int seedInt = stoi(argv[2]);
		genSaveToBinSVG(seedInt, argv[3], argv[4], argv[6], argv[8]);
	}

   // generate maze with time based seed, width and height values, 
	// save to binary file and svg file
	// ./mazer --g seed width height --sb filename.maze --sv filename.svg
	else if(str5.compare(argv[1]) == 0 && str3.compare(argv[4]) == 0 
		  && str4.compare(argv[6]) == 0 && argc == 8)
	{
		// mersenne twister seeded with random number based on current time
		mt19937 mt_rand(time(0));
		unsigned int seedInt = mt_rand();
		cout << "Time based seed is: " << seedInt << endl;
		genSaveToBinSVG(seedInt, argv[2], argv[3], argv[5], argv[7]);
	}

	// if user does not enter a seed value, then generate seed based 
	// on current time and plug into maze generator
	else if(str5.compare(argv[1]) == 0 && argc == 4)
	{
		// mersenne twister seeded with random number based on current time
		mt19937 mt_rand(time(0));
		unsigned int seed = mt_rand();
		cout << "Time based seed is: " << seed << endl;
	}

	// exit program if all command line args fail
	else
	{
		printError();
	}
}

// generate maze with seed, width and height values, save to binary file
void CommandLine::genSaveToBin(unsigned int& seed, string width, 
						       string height, string fileName)
{
	BinaryGenerator bg;
	Binary b;

	// convert command line args to ints
	unsigned int widthInt = stoi(width);
	unsigned int heightInt = stoi(height);

	// create a maze object for the generateMaze function
	Maze m(widthInt, heightInt);

	vector<Edge::EdgeType> edges = bg.generateMaze(seed, m); 
	b.writeBinaryFile(m, edges, fileName);
}

// load binary file and save to svg file 
void CommandLine::loadBinSaveToSVG(string binaryFile, string svgfFileName)
{
	Maze m;
	SVG s;

	vector<Edge::EdgeType> edges = m.loadMaze(binaryFile);
	unsigned int width = m.getWidth();
	unsigned int height = m.getHeight();
	s.saveToSVG(edges, width, height, svgfFileName);
}

// generate with seed, width and height values, save to svg file
void CommandLine::genSaveToSVG(unsigned int& seed, string width, 
						       string height, string svgFileName)
{
	BinaryGenerator bg;
	SVG s;

	// convert command line args to ints
	unsigned int widthInt = stoi(width);
	unsigned int heightInt = stoi(height);

	// create a maze object for the generateMaze function
	Maze m(widthInt, heightInt);

	vector<Edge::EdgeType> edges = bg.generateMaze(seed, m); 
	s.saveToSVG(edges, widthInt, heightInt, svgFileName);
}

// generate maze with seed, width and height values, 
// save to binary file and svg file
void CommandLine::genSaveToBinSVG(unsigned int& seed, string width, 
						       string height, string fileName, string svgFileName)
{
	BinaryGenerator bg;
	Binary b;
	SVG s;

	// convert command line args to ints
	unsigned int widthInt = stoi(width);
	unsigned int heightInt = stoi(height);

	// create a maze object for the generateMaze function
	Maze m(widthInt, heightInt);

	vector<Edge::EdgeType> edges = bg.generateMaze(seed, m); 
	b.writeBinaryFile(m, edges, fileName);
	s.saveToSVG(edges, widthInt, heightInt, svgFileName);
}

// printError notifies the user of invalid input
void CommandLine::printError()
{
	string message = "Invalid args, usage:\n"
					     "--lb, Load Binary file: filename.maze\n"
					     "--sb, Save Binary file: filename.maze\n"
					     "--sv, Save SVG file: filename.svg\n"
					     "--g, Generate maze: seed width height\n";
	cout << message;
}
