/****************************************************************************
* COSC1252/1254 Programming Using C++
* SP1 2016 Assignment #1 - Mazes
* Full Name        : Paolo Di Pietro
* Student Number   : s3427174
****************************************************************************/
#include "binaryGenerator.h"

vector<Edge::EdgeType> BinaryGenerator::generateMaze(unsigned int& seed, 
																						Maze m)
{
	Cell c;
	Edge::EdgeType edge;

	std::mt19937 rng(seed);
	std::uniform_int_distribution<int> uni(0, 1);

	unsigned int width = m.getWidth(); 
	unsigned int height = m.getHeight(); 

	c.initCellCoords(width, height);

	vector<vector<Cell>> cells = c.getCellVector();
	vector<Edge::EdgeType> edgeStructs = m.getEdgeVector();

	for(auto r = cells.begin(); r != cells.end(); ++r)
	{
	   for(auto c = r->begin(); c != r->end(); ++c)
	   {  
	   	// at each cell, generate either a 0 or 1.
	   	// 0 for east and 1 for south 
	   	unsigned int randomInt = uni(rng);

	   	// if true, get the coords of the current cell and the coords
	   	// of the eastern neighbour
   		if(randomInt == east)
   		{
   			// if no neighbouring cell, assign southern cell coordinates
   			// to create vertical edge
   			if(c->getAdjCellType().xEast == 0 && 
   				c->getAdjCellType().yEast == 0)
   			{
   				edge.x1 = c->getX();
	   			edge.y1 = c->getY();
	   			edge.x2 = c->getAdjCellType().xSouth;
	   			edge.y2 = c->getAdjCellType().ySouth;
	   			edgeStructs.push_back(edge);
   			}
   			// else create a horizontal edge
   			else
   			{
	   			edge.x1 = c->getX();
	   			edge.y1 = c->getY();
	   			edge.x2 = c->getAdjCellType().xEast;
	   			edge.y2 = c->getAdjCellType().yEast;
	   			edgeStructs.push_back(edge);
	   		}
   		}
   		// if true, get the coords of the current cell and the coords
	   	// of the southern neighbour
   		if(randomInt == south)
   		{
   			// if no neighbouring cell, assign eastern cell coordinates
   			// to create horizontal edge
   			if(c->getAdjCellType().xSouth == 0 && 
   				c->getAdjCellType().ySouth == 0)
   			{
   				edge.x1 = c->getX();
	   			edge.y1 = c->getY();
	   			edge.x2 = c->getAdjCellType().xEast;
	   			edge.y2 = c->getAdjCellType().yEast;
	   			edgeStructs.push_back(edge);
   			}
   			// else create a vertical edge
   			else
   			{
	   			edge.x1 = c->getX();
	   			edge.y1 = c->getY();
	   			edge.x2 = c->getAdjCellType().xSouth;
	   			edge.y2 = c->getAdjCellType().ySouth;
	   			edgeStructs.push_back(edge);
	   		}
   		}
	   }
	}
	return edgeStructs;
}
