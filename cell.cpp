/****************************************************************************
* COSC1252/1254 Programming Using C++
* SP1 2016 Assignment #1 - Mazes
* Full Name        : Paolo Di Pietro
* Student Number   : s3427174
****************************************************************************/
#include "cell.h"

// initCellCoords function initializes the cells in the maze with their 
// corresponding x,y position on the grid
void Cell::initCellCoords(unsigned int& width, unsigned int& height)
{
	for(unsigned int i = 0; i < height; i++) 
	{
		// Create an empty row
      vector<Cell> row; 

      for(unsigned int j = 0; j < width; j++) 
      {
      	Cell myCell;
      	// set x position of the cell
      	myCell.x = j;
      	// set y position of the cell
      	myCell.y = i;

      	// add an element (column) to the row
         row.push_back(myCell); 
      }     
    	// Add the row to the main vector
      cells.push_back(row); 
   }
   getAdjacentCell(width, height, cells);
}

// getAdjacentCell function gets the coordinates of all the eastern cells
// until the end is reached and pushes all the data into a 1D vector to be 
// used for populating each adjacency list struct for each cell
void Cell::getAdjacentCell(unsigned int& width, 
	 								unsigned int& height, 
	 								vector<vector<Cell>>& cells)
{
	Cell *p1, *p2;
	unsigned int index = 0;
	// this vector holds the coordinates of all adjacent cells
   vector<int> adjCells; 

   for(vector<Cell> &v : cells)
	{
		p2 = &v.front();

		// we start from the first element in the first row 
	   for(unsigned int i = 0; i < width; i++)
	   {  
	   	// if we are at the first element of the first row, get the first 
	   	// neighbouring cell to the east and increment through the row until
	   	// the end is reached
	   	if(index < width - 1 && p2->x == 0 && p2->y == 0)
	   	{
		   	p1 = &v.at(index + 1); 
		   	index++;
		   	adjCells.push_back(p1->x);
		   	adjCells.push_back(p1->y);
		   } 
	   }
	   // reset the index counter to 0
	   index = 0;

	   for(unsigned int i = 0; i < width; i++)
	   {  
	   	// if the Y coord is not equal to zero, then we know that it's not 
	   	// the first row, we then increment to every element in each row to
	   	// to get it's corresponding neighbouring cell
		   if(index < width && p2->y != 0)
	   	{
		   	p1 = &v.at(index); 
		   	index++;
		   	adjCells.push_back(p1->x);
		   	adjCells.push_back(p1->y);
		   } 
	   }
	   // reset the index counter to 0
	   index = 0;
	}
	initAdjList(adjCells, cells, width, height);
}	

// initAdjList initializes the stuct in each cell object with the coordinates
// of it's eastern and southern neighbouring cells
void Cell::initAdjList(vector<int>& adjCells, 
							  vector<vector<Cell>>& cells, 
							  unsigned int& width, unsigned int& height)
{	
	static unsigned int i = 0;

	for(auto r = cells.begin(); r != cells.end(); ++r)
	{
	   for(auto c = r->begin(); c != r->end(); ++c)
	   {
	   	// here we get both eastern and southern neighbouring cells to 
	   	// to push into adjacency lists
	   	if(next(c) != r->end() && next(r) != cells.end())
	   	{
		   	c->myAdjacentCell.xEast = adjCells[i];
		   	c->myAdjacentCell.yEast = adjCells[i + 1];
		   	c->myAdjacentCell.xSouth = adjCells[i + ((2 * width) - 2)];
		   	c->myAdjacentCell.ySouth = adjCells[i + ((2 * width) - 1)];
		   }
	   	// if we reach the end of a row, the maze can only go down so we
	   	// assign the coords of the downward neighbour and 0's in the east 
	   	// x,y of the adjacency list because there is no eastern neighbour
	   	if(next(c) == r->end())
	   	{
	   		c->myAdjacentCell.xEast = 0;
		   	c->myAdjacentCell.yEast = 0;
	   		c->myAdjacentCell.xSouth = adjCells[i + ((2 * width) - 2)];
	   	   c->myAdjacentCell.ySouth = adjCells[i + ((2 * width) - 1)];
	   	}
	   	// if we reach the last row, the maze can only go right so we assign
	   	// the coords of the east neighbouring cell and 0's into the southern
	   	// x,y of the adjacency list because there is no southern neighbour
	   	if(next(r) == cells.end())
	   	{
	   		c->myAdjacentCell.xEast = adjCells[i];
		   	c->myAdjacentCell.yEast = adjCells[i + 1];
		   	c->myAdjacentCell.xSouth = 0;
		   	c->myAdjacentCell.ySouth = 0;
	   	}
	   	// if both the inner and outer loops reach the end, aka we reach
	   	// the bottom right corner, the maze can't neither go right 
	   	// or down, so we assign width and height minus 1 to stop it going 
	   	// over the edges (yes this is a bit hacky)
	   	if(next(c) == r->end() && next(r) == cells.end())
	   	{
	   		c->myAdjacentCell.xEast = width - 1;
		   	c->myAdjacentCell.yEast = width - 1;
		   	c->myAdjacentCell.xSouth = height - 1;
		   	c->myAdjacentCell.ySouth = height - 1;
	   	}
	   	// increment index variable by 2
	   	i = i + 2;
	   }
	}
}

// printCells function iterates through the 2D vector of Cell objects 
// and prints the coordinates of each cell and the coordinates of the eastern
// southern neighbouring cells for debugging purposes
void Cell::printCells(vector<vector<Cell>>& cells)
{
   for(const vector<Cell> &v : cells)
	{
	   for(Cell cell : v)
	   {  
	   	cout << cell.x 
	   	     << cell.y 
	   	     << ' '
	   	     << cell.myAdjacentCell.xEast 
	   	     << cell.myAdjacentCell.yEast 
	   	     << cell.myAdjacentCell.xSouth 
	   	     << cell.myAdjacentCell.ySouth 
	   	     << ' ';	
	   }
	   cout << endl;
	}
}
