#############################################################################
# COSC1252/1254 Programming Using C++
# SP1 2016 Assignment #1 - Mazes
# Full Name        : Paolo Di Pietro
# Student Number   : s3427174
#############################################################################

OBJS = main.o maze.o cell.o binaryGenerator.o svg.o binary.o commandLine.o
CC = g++
DEBUG = -g
CFLAGS = -Wall -c -std=c++14 $(DEBUG)
LFLAGS = -Wall -pedantic $(DEBUG)

mazer : $(OBJS)
	$(CC) $(LFLAGS) $(OBJS) -o mazer

main.o : main.cpp maze.h cell.h binaryGenerator.h svg.h \
	binary.h commandLine.h
	$(CC) $(CFLAGS) main.cpp

maze.o : maze.h maze.cpp cell.h binaryGenerator.h svg.h \
	binary.h commandLine.h
	$(CC) $(CFLAGS) maze.cpp

cell.o : cell.h cell.cpp binaryGenerator.h svg.h binary.h commandLine.h
	$(CC) $(CFLAGS) cell.cpp

binaryGenerator.o : binaryGenerator.h binaryGenerator.cpp svg.h \
	binary.h commandLine.h
	$(CC) $(CFLAGS) binaryGenerator.cpp

svg.o : svg.h svg.cpp binary.h commandLine.h
	$(CC) $(CFLAGS) svg.cpp

binary.o : binary.h binary.cpp commandLine.h
	$(CC) $(CFLAGS) binary.cpp

commandLine.o : commandLine.h commandLine.cpp
	$(CC) $(CFLAGS) commandLine.cpp

clean:
	\rm -f *.o *~ mazer
