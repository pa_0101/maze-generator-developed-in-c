/****************************************************************************
* COSC1252/1254 Programming Using C++
* SP1 2016 Assignment #1 - Mazes
* Full Name        : Paolo Di Pietro
* Student Number   : s3427174
****************************************************************************/
#pragma once

class Edge {
	
private:

	typedef struct edge {
		unsigned int x1;
		unsigned int y1;
		unsigned int x2;
		unsigned int y2;
	} EdgeType;

	EdgeType myEdgeType;

	// give the classes below access to the private struct
	friend class CommandLine;
	friend class Maze;
	friend class Binary;
	friend class BinaryGenerator;
	friend class SVG;

public:

	Edge() {}
};
